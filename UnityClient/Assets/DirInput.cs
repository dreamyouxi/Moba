﻿using UnityEngine;
using System.Collections;


public class Entity1 : Model
{

    public override void Update()
    {
        base.Update();
    }

    public int value
    {
        get
        {
            return this._value;
        }
        set
        {
            this._value = value;
        }
    }
    public float x
    {
        set
        {
            this._x = value;
        }
        get
        {
            return this._x;
        }
    }

    public float y
    {
        set
        {
            this._y = value;
        }
        get
        {
            return this._y;
        }
    }
    public int dir
    {
        set
        {
            this._dir = value;
        }
        get
        {
            return this._dir;
        }
    }

    public int no
    {
        get
        {
            return this._no;
        }
        set
        {
            this._no = value;
        }
    }
    private int _value = 1;
    private float _x = 0.0f;
    private float _y = 0.0f;
    private int _dir = -1;
    private int _no = 0;

}


public class DirInput : Entity1
{
    public float rotateZ_arrow;
    public float x_center;
    public float y_center;

    public float x_arrow;
    public float y_arrow;

    public float x_bg;
    public float y_bg;

    public Vector2 pos_began;






    public override bool Init()
    {
        base.Init();
        ins = this;
        ///   ModelMgr.ins.Add(this as Model);

  //      EventDispatcher.ins.AddEventListener(this, Event.ID_UI_WAIT);
      //  EventDispatcher.ins.AddEventListener(this, Event.ID_UI_NOWAIT);
       
        return true;
    }



    public static DirInput ins = null;





    bool isTouch = false;

    // private int dir = -1;
    private int Degree = -1;

    public bool isLight;

    public override void OnEvent(int type, object userData)
    {
      /*  if (type == Event.ID_ABLE_DIRINPUT)
        {
            this.enable = true;
        }
        else if (type == Event.ID_DISABLE_DIRINPUT)
        {
            this.enable = false;
        }

        switch (type)
        {
            case Event.ID_UI_WAIT:
                {
                    this.enable = true;

                } break;
            case Event.ID_UI_NOWAIT:
                {
                    this.enable = false;
                } break;

        }
        */
    }



    private bool enable = true
;

    void OnTouchBegan(float x, float y)
    {
        this.SetPosition(pos_began);
    }

    void SetPosition(Vector2 pos)
    {
        //if (float.IsNaN(pos.x) == false && float.IsNaN(pos.y) == false)
        {
            Vector2 v = ConvertToUIVector2(pos);
            this.x = v.x;
            this.y = v.y;

            x_center = this.x;
            y_center = this.y;

            x_arrow = 0.0f;//this.x;
            y_arrow = 0.0f; //this.y;

            x_bg = x;
            y_bg = y;
        }
    }

    void OnTouchEnded(float x, float y)
    {

        this._enable = false;
        //  dir = -1;
        SetPosition(pos_began);
    }

    private Vector2 ConvertToUIVector2(Vector2 pos)
    {
        pos.x -= Screen.width / 2.0f;
        pos.y -= Screen.height / 2.0f;
        return pos;
    }
    void OnTouchMoved(float x, float y)
    {
        this._enable = true;

        //8方向 0 1 2 3 4 5 6 7

        const float DELTA_XY = 30.0f;//图片移动范围
        const float ONE_DEGREE = DATA.ONE_DEGREE;//一度的弧度

        float dx = x - (pos_began.x);
        float dy = y - (pos_began.y);
        float dposx = 0.0f, dposy = 0.0f;

        int dir = this.dir;//方向
        float degree = Mathf.Atan(dy / dx) * 57.29578f;

        if (float.IsNaN(degree)) return;
        float degree_total = 0.0f;

        if (dx == 0.0f || dy == 0.0f)
        {
            //return;
        }

        float half_half = 45.0f / 2.0f;

        if (dx >= 0.0f && dy >= 0.0f)// 0 1 2 //1
        {
            if (degree < half_half) dir = 0;
            else if (degree > half_half + 45.0f) dir = 2;
            else dir = 1;
            dposy = DELTA_XY * Mathf.Sin(degree * ONE_DEGREE);
            dposx = DELTA_XY * Mathf.Cos(degree * ONE_DEGREE);
            degree_total = degree;
        }
        else if (dx >= 0.0f && dy <= 0.0f)// 0 7 6 //4
        {
            degree = -degree;
            if (degree < half_half) dir = 0;
            else if (degree > half_half + 45.0f) dir = 6;
            else dir = 7;
            dposy = -DELTA_XY * Mathf.Sin(degree * ONE_DEGREE);
            dposx = DELTA_XY * Mathf.Cos(degree * ONE_DEGREE);

            degree_total = 360.0f - degree;
        }
        else if (dx <= 0.0f && dy >= 0.0f)// 2 3 4 //2
        {
            degree = -degree;
            if (degree < half_half) dir = 4;
            else if (degree > 45.0f + half_half) dir = 2;
            else dir = 3;
            dposy = DELTA_XY * Mathf.Sin(degree * ONE_DEGREE);
            dposx = -DELTA_XY * Mathf.Cos(degree * ONE_DEGREE);
            degree_total = 180.0f - degree;
        }
        else//4 5 6 // 3
        {
            if (degree < half_half) dir = 4;
            else if (degree > 45 + half_half) dir = 6;
            else dir = 5;
            dposy = -DELTA_XY * Mathf.Sin(degree * ONE_DEGREE);
            dposx = -DELTA_XY * Mathf.Cos(degree * ONE_DEGREE);
            // degree_total = 180 + degree;
            degree_total = 180.0f + degree;
        }


        if (float.IsNaN(dposx) == false || float.IsNaN(dposy) == false)
        {
            Vector2 v = ConvertToUIVector2(new Vector2(pos_began.x + dposx, pos_began.y + dposy));
            this.x_center = v.x;
            this.y_center = v.y;



            //   float[] pos_x = { 42.0f, 37.0f, 0.0f, -37.0f, -42f, -37.0f, 0.0f, 37.0f };
            //   float[] pos_y = { 0.0f, 37.0f, 42.0f, 37.0f, 0.0f, -37.0f, -42f, -37.0f };




            y_arrow = 50 * Mathf.Sin(degree_total * ONE_DEGREE);// *yy[d / 90];
            x_arrow = 50 * Mathf.Cos(degree_total * ONE_DEGREE);// *xx[d / 90];



            rotateZ_arrow = degree_total - 270.0f;
            this.dir = dir;
            this.Degree = (int)degree_total;

            PublicData.ins.dir = this.Degree;

        }
        else
        {
            y_arrow = 0.0f;
            x_arrow = 0.0f;
            rotateZ_arrow = 0.0f;
            Debug.Log("invalid  value");

        }


    }



    public override void Update()
    {
     

     ///   if (enable) return;
        /*   Ball self = BallsMgr.ins.GetSelfBalls();
           if(self.alive)
           {

           }
           else
           {
               this._enable = false;
               return;
           }*/

        do
        {
            if (Input.GetButton("Fire1") && isTouch == false)
            {
                float x = Input.mousePosition.x, y = Input.mousePosition.y;

                // pos_began.y =( Input.mousePosition.y+42) / Screen.height * 10.0f;
                //   pos_began.x = (Input.mousePosition.x+43 )/ Screen.width * 21.0f;

                //   pos_began = new Vector3(x - Screen.width / 2.0f, y - Screen.height / 2.0f, 0.0f);

                pos_began = new Vector2(x, y);

                isTouch = true;
                this.OnTouchBegan(x, y);
            }
            if (Input.GetButton("Fire1") == false && isTouch == true)
            {
                if (isTouch == true)
                {
                    this.OnTouchEnded(Input.mousePosition.x, Input.mousePosition.y);

                    isTouch = false;
                }
            }

            if (isTouch == true)
            {
                if (Input.mousePosition.x != pos_began.x && Input.mousePosition.y != pos_began.y)
                {
                    this.OnTouchMoved(Input.mousePosition.x, Input.mousePosition.y);
                }
            }

        } while (false);

    }



    public int GetDirection()
    {
        return dir;
    }
    public int GetDirectionDegree()
    {
        return this.Degree;
    }
    public void SetEnable(bool b)
    {

    }
    public bool _enable = true;




}

